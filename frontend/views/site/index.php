<?php
use frontend\controllers\SiteController;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Умное решение';
?>
<? if (Yii::$app->session->hasFlash('successSendCp')):?>
<div class="container-fluid bg-white">
    <div class="container contacts-wrapper">
        <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">Ваш запрос на купон отправлен!</h4>
            <p>Ваша заявка будет рассмотрена в ближайшее время.</p>
            <hr>
            <p class="mb-0">С Вами свяжется наш менеджер.</p>
        </div>
    </div>
</div>
    <? Yii::$app->session->removeFlash('successSendCp')?>

<?elseif(Yii::$app->session->hasFlash('errorSendCp')):?>
<div class="container-fluid bg-white">
    <div class="container contacts-wrapper">
        <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Произошла ошибка!</h4>
            <p>При отправке Вашего вопроса возникла ошибка, попробуйте еще раз.</p>
            <hr>
            <p class="mb-0">Мы устраним данную проблему как можно скорее.</p>
        </div>
    </div>
</div>
    <? Yii::$app->session->removeFlash('errorSendCp')?>
<?else:?>
<main>
    <div class="container-fluid promotion d-flex justify-content-center align-items-center">
        <div class="prom-text d-flex align-items-center flex-column">
            <h1 class="title-desc">Умное решение</h1>
            <p class="title-desc">Производство домокомплектов</p>
            <p class="title-desc">для самостоятельной сборки дома</p>
            <p class="text-coupon one-text title-desc">Летняя акция!</p>
            <p class="text-coupon two-text title-desc">домокомплект Монэ всего за 300т.р.</p>
            <a href="#coupon-form" class="write-us-btn d-flex justify-content-center align-items-center popup-with-form">Оформить заказ</a>
        </div>
        <div class="prom-social d-flex justify-content-center flex-column align-items-center">
            <a href="https://www.facebook.com/umnoe.reshenie" target="_blank">
                <span class="instagram"></span>
            </a>
            <a href="https://vk.com/public127208078" target="_blank">
                <span class="vk"></span>
            </a>
            <a href="https://www.instagram.com/a_frame_solution/" target="_blank">
                <span class="whatsup"></span>
            </a>
        </div>
    </div>
    <div class="container-fluid bg-white">
        <div class="container house flex-column align-items-center d-flex">
            <div class="title-wrap align-items-center d-flex flex-column">
                <div class="decoration">
                    <div></div>
                    <div></div>
                </div>
                <h2>Строительство дома под ключ!</h2>
                <p>Купить дом дешево - Умное решение!</p>
            </div>
            <div class="house-content d-flex">
                <div class="img-wrap">
                    <img src="/img/a-frame.png" width="580" height="380" alt="house-plan">
                </div>
                <div class="house-content-wrap">
                    <h3>Ваш будущий дом</h3>
                    <p>Должен быть приспособлен для круглогодичного проживания, но вы не готовы тратить
                        на это целое состояние и половину жизни, тогда наше предложение для вас.
                        Мы предлагаем вам заказать стильный, красивый и очень удобный дом
                        из нашего домокомплекта «Монэ» в стиле A-frame.</p>
                    <p>Наши домокомплекты создаются по принципу мебели ИКЕЯ,
                        все детали подогнаны, сделаны в размер и покрашены защитным составом, это всегда
                        получается дешевле и качественней, чем строительство готового дома
                        непосредственно на участке.
                        Построить готовый дом под ключ из
                        нашего домокомплекта немного сложней, чем собрать шкаф.
                    </p>
                </div>

            </div>
        </div>
        <div class="container variants-house flex-column align-items-center">
            <div class="variants-container flex-wrap d-flex justify-content-center">
                <div class="card card-variant" style="width: 18rem;">
                    <div class="card-body align-items-center d-flex flex-column">
                        <a class="navbar-brand img-card" href="#">
                            <img src="/img/logo.svg" width="95" height="50" alt="">
                            <h1>Умное Решение</h1>
                        </a>
                        <h5 class="card-title">Экономно</h5>
                        <p class="card-text">Дом типа A-frame дешевле других. В качестве фундамента будет достаточно 12 свай (диаметром 108 мм)</p>
                    </div>
                </div>
                <div class="card card-variant" style="width: 18rem;">
                    <div class="card-body align-items-center d-flex flex-column">
                        <a class="navbar-brand img-card" href="#">
                            <img src="/img/logo.svg" width="95" height="50" alt="">
                            <h1>Умное Решение</h1>
                        </a>
                        <h5 class="card-title">Быстро</h5>
                        <p class="card-text">При сильном желании и помощи родствеников, либо друзей можно собрать дом за 2 дня.</p>
                    </div>
                </div>
                <div class="w-100"></div>
                <div class="card card-variant" style="width: 18rem;">
                    <div class="card-body align-items-center d-flex flex-column">
                        <a class="navbar-brand img-card" href="#">
                            <img src="/img/logo.svg" width="95" height="50" alt="">
                            <h1>Умное Решение</h1>
                        </a>
                        <h5 class="card-title">Без нервов и лишних трудозатрат</h5>
                        <p class="card-text">Вам остается собрать детали домокомплекта по инструкции - строительное оборудование вам не понадобится (Только молоток и лестница)</p>
                    </div>
                </div>
                <div class="card card-variant" style="width: 18rem;">
                    <div class="card-body align-items-center d-flex flex-column">
                        <a class="navbar-brand img-card" href="#">
                            <img src="/img/logo.svg" width="95" height="50" alt="">
                            <h1>Умное Решение</h1>
                        </a>
                        <h5 class="card-title">Надежно и функционально</h5>
                        <p class="card-text">Дом требует минимального ухода, так как стены дома являются одновременно кровлей и не требуют регулярной покраски.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container sale-container flex-column align-items-center d-flex">
            <div class="title-wrap align-items-center d-flex flex-column">
                <div class="decoration">
                    <div></div>
                    <div></div>
                </div>
                <h2>Что мы предлагаем?</h2>
                <p>Самый дешевый способ построить собственный дом</p>
            </div>
            <div class="card-container w-100 d-flex flex-wrap justify-content-around">
                <div class="card card-sale d-flex align-items-center">
                    <div class="card-body card-body-one d-flex align-items-center flex-column">
                        <a class="navbar-brand img-card" href="#">
                            <img src="/img/logo.svg" width="95" height="50" alt="">
                            <h1>Умное Решение</h1>
                        </a>
                        <h5 class="card-title">Самостоятельное <span>строительство</span></h5>
                        <h3>300</h3>
                        <h4>тыс. руб</h4>
                        <p class="card-text">Строительство удобного и красивого дома в стиле A-frame
                            площадью 54 кв метра, из домокомплекта «Монэ»</p>
                    </div>
                    <a href="#home-self" class="more-btn open-popup-link">Подробнее &raquo;</a>
                </div>
                <div class="card card-sale d-flex align-items-center">
                    <div class="card-body card-body-one d-flex align-items-center flex-column">
                        <a class="navbar-brand img-card" href="#">
                            <img src="/img/logo.svg" width="95" height="50" alt="">
                            <h1>Умное Решение</h1>
                        </a>
                        <h5 class="card-title">Строительство <br><span>под ключ</span></h5>
                        <h3>600</h3>
                        <h4>тыс. руб</h4>
                        <p class="card-text">Цена строительства дома площадью 54 кв метра
                            под ключ, с утеплителем 200 мм, крышей
                            из гибкой черепицы.</p>
                    </div>
                    <a href="#home-full" class="more-btn open-popup-link">Подробнее &raquo;</a>
                </div>

            </div>
        </div>
    </div>
    <div class="gallery container-fluid">
        <div class="row justify-content-center">
            <div class="title-wrap align-items-center d-flex flex-column">
                <div class="decoration">
                    <div></div>
                    <div></div>
                </div>
                <h2>Наши работы!</h2>
                <p>Примеры домов построенных из домокомплекта "Монэ"</p>
            </div>
        </div>
        <div class="row">
            <? foreach ($modelGallery as $item):?>
                <div class="img-wrapper">
                    <a href="<?=$item->img?>" class="item">
                        <img src="<?=$item->img?>" alt="">
                    </a>
                </div>
            <?endforeach;?>

        </div>

        <div class="read-all d-flex justify-content-center">
            <a href="<?=Yii::$app->urlManager->createUrl('/gallery')?>">Смотреть все фотографии &raquo;</a>
        </div>
    </div>


    <div class="container-fluid bg-white">
        <div class="news-container container">
            <div class="title-wrap align-items-center d-flex flex-column">
                <div class="decoration">
                    <div></div>
                    <div></div>
                </div>
                <h2>Умный блог</h2>
                <p>Узнай больше о строительстве на нашем сайте </p>
            </div>

            <div class="card-container w-100 d-flex flex-wrap justify-content-around">
                <? foreach ($modelArticle as $item):?>
                    <?$text = mb_substr($item->short, 0, 130, 'UTF-8').'...'?>
                    <? $date = explode("-",$item->date)?>

                <div class="card card-news d-flex">
                    <div class="img-news-wrapper">
                    <img class="card-img-top" src="<?= $item->img?>" alt="Card image cap">
                    </div>
                    <span class="badge data d-flex justify-content-center align-items-center flex-column">
                        <span><?=$date[2]?></span>
                        <span><?=SiteController::dateMonth($date[1])?></span>
                    </span>
                    <div class="card-body">
                        <h3><?= $item->title?></h3>
                        <p class="card-text">
                            <?=$text?>
                        </p>
                    </div>
                    <a href="<?=Yii::$app->request->url."?article=".$item->id?>">Читать полностью &raquo;</a>
                </div>
                <?endforeach;?>
            </div>

            <div class="read-all d-flex justify-content-center">
                <a href="<?=Yii::$app->urlManager->createUrl('/post')?>">Читать другие статьи &raquo;</a>
            </div>
        </div>
    </div>

    <div id="home-self" class="white-popup mfp-hide">
        <div class="about-house flex-column align-items-center d-flex container bg-white">
            <div class="title-wrap align-items-center d-flex flex-column">
                <div class="decoration">
                    <div></div>
                    <div></div>
                </div>
                <h2><span><b>СОСТАВ ДОМОКОМПЛЕКА</b></span></h2>
            </div>
            <div class="content-about">
                <h4 style="text-align: center">
                    В состав домокомплекта «Монэ» для самостоятельного строительства дома
                    в стиле A-frame входят материалы и столярные изделия, необходимые для создания
                    теплового контура дома площадью 54 кв. метра по первому этажу, для его последующего утепления и отделки.
                    Размер фундамента 6х9 метров
                </h4>
                <p>Фронтоны из профилированного бруса 200х150 с оконным и дверным проемом для установки стеклопакетов и дверей с «откосячкой».</p>
                <p>Стропильная система с шагом установки 600 мм из доски 200х50</p>
                <p>Потолочные балки для устройства второго этажа из доски 200х50</p>
                <p>Обрешетка стропильной системы из доски 100х25</p>
                <p> Домокомплект обработан бесцветным защитным составом, уменьшающим растрескивание бруса, и исключающим поражение его плесенью и насекомыми.
                <p>Инструкция для самостоятельной сборки домокомплекта «Монэ»</p>
                <h4>Более подробную информацию о строительстве дома из домокомплекта
                    «Монэ»вы можете получить позвонив по телефону</h4>
            </div>
            <img src="/img/1527778212.jpg" alt="">
            <a href="<?= Yii::$app->urlManager->createUrl('/contacts')?>" class="write-us-btn margin-btn d-flex justify-content-center align-items-center">Связаться с нами</a>
        </div>
    </div>
    <div id="home-full" class="white-popup mfp-hide">
        <div class="about-house flex-column align-items-center d-flex container bg-white">
            <div class="title-wrap align-items-center d-flex flex-column">
                <div class="decoration">
                    <div></div>
                    <div></div>
                </div>
                <h2><span><b>КОМПЛЕКТАЦИЯ ДОМА</b></span></h2>
            </div>
            <div class="content-about">
                <h4 style="text-align: center">
                    В состав готового дома в стиле A-frame под чистовую
                    отделку из домокомплекта «Монэ» входят материалы и столярные изделия,
                    необходимые для создания теплового контура дома площадью 56 кв. метра.
                    Размер фундамента 6х9 метров
                </h4>
                <p>Наша компания произведет доставку и сборку домокомплекта «Монэ» на вашем участке, проведет его утепление по всему тепловому контуру 200 мм базальтовой плитой, установку кровельного покрытия, отделку карнизных свесов, конька и фронтонных планок.</p>
                <p>После чего у вас будет полностью готовый для чистовой отделки дом, пригодный для круглогодичного проживания.</p>
                <p>Домокомплект будет обработан бесцветным защитным составом, уменьшающим растрескивание бруса, и исключающим поражение его плесенью и насекомыми.
                <p>Более подробную информацию о строительстве дома из домокомплекта «Монэ» вы можете получить позвонив по телефону.</p>
            </div>
            <img src="/img/1527778237.JPG" alt="">
            <a href="<?= Yii::$app->urlManager->createUrl('/contacts')?>" class="write-us-btn margin-btn d-flex justify-content-center align-items-center">Связаться с нами</a>
        </div>
    </div>
    <? $form = ActiveForm::begin(['id' => 'coupon-form', 'options' => [
        'class' => 'white-popup mfp-hide d-flex flex-column align-items-center'
    ]]);?>
    <div class="form-wrapper">
        <h1>Получить купон</h1>
        <small id="textHelp" class="form-text mini-text-form text-muted">Заполните информацию ниже</small>
        <div class="form-group">
            <?= $form->field($model, 'name')->textInput(['class'=>'form-control', 'autofocus' => true, 'placeholder' => 'Ваше имя'])->label('Ваше имя')?>
        </div>
        <div class="form-group">
            <?= $form->field($model, 'mail')->textInput(['class'=>'form-control', 'placeholder' => 'Ваш email'])->label('Ваш email')?>
            <small id="emailHelp" class="form-text text-muted">Мы некогда не передадим Ваш email в руки мошенников</small>
        </div>
        <div class="form-group">
            <?= $form->field($model, 'phone')->textInput(['class'=>'form-control', 'placeholder' => 'Ваш телефон'])->label('Ваш телефон')?>
        </div>
    </div>
    <?=Html::submitButton('Отправить', ['class' => 'write-us-btn send-contact submit-form'])?>
    <? ActiveForm::end();?>
    <?endif;?>
