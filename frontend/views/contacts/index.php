<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
/**
 * Created by PhpStorm.
 * User: dmitrijlebedev
 * Date: 27.05.18
 * Time: 21:40
 */
/* @var $model \frontend\models\ContactForm */
?>

<div class="container-fluid bg-white">
        <div class="container contacts-wrapper">
            <? if (Yii::$app->session->hasFlash('successSend')):?>
                <div class="alert alert-success" role="alert">
                    <h4 class="alert-heading">Ваш вопрос принят на рассмотрение!</h4>
                    <p> Ваш вопрос будет рассмотрен в течении нескольких часов.</p>
                    <hr>
                    <p class="mb-0">Мы обязательно Вам перезвоним!</p>
                </div>
                <? Yii::$app->session->removeFlash('successSend')?>

            <?elseif(Yii::$app->session->hasFlash('errorSend')):?>

                <div class="alert alert-danger" role="alert">
                    <h4 class="alert-heading">Произошла ошибка!</h4>
                    <p> При отправке Вашего вопроса возникла ошибка, попробуйте еще раз.</p>
                    <hr>
                    <p class="mb-0">Мы устраним данную проблему как можно скорее.</p>
                </div>

                <? Yii::$app->session->removeFlash('errorSend')?>
            <?else:?>
            <div class="row">
                <div class="info-wrapper col-lg-6">
                    <p class="number-phone">8 (953) 668 81 38 (Кострома)</p>
                    <p class="number-phone">8 (926) 273 99 20 (Москва)</p>
                    <p class="what-number-contact" style="color: #000;" href="https://api.whatsapp.com/send?phone=79262739920" target="_blank">8 (926) 273 99 20 (Whatsapp)</p>
                    <p class="mail-letter">sale@umnye.com</p>
                    <p class="address">г.Кострома Вологодское шоссе СТ Дружба участок №1
                    (Офис и выставочный образец)<a href="#office" class="see-map open-popup-link">Смотреть карту</a></p>
                    <p class="address">г. Кострома ул. Заречная д.15 завод “Стройдеталь”
                        (Производство) <a href="#production" class="see-map open-popup-link">Смотреть карту</a></p>
                </div>
                <? $form = ActiveForm::begin(['id' => 'contact-form', 'options' => [
                    'class' => 'form-contacts col-lg-6'
                ]]);?>
                <div class="title-wrap d-flex flex-column">
                    <h2><span>Остались</span> вопросы?</h2>
                    <p>Отправь нам сообщение и мы ответим в течении часа!</p>
                </div>

                <div class="row">
                    <div class="col">
                        <?= $form->field($model, 'name')->textInput(['class'=>'form-control', 'autofocus' => true, 'placeholder' => 'Ваше имя'])->label(false)?>
                    </div>
                    <div class="col">
                        <?= $form->field($model, 'phone')->textInput(['class'=>'form-control', 'placeholder' => 'Ваш номер'])->label(false)?>
                    </div>
                </div>
                <?=$form->field($model, 'body')->textarea(['rows'=>6, 'class'=>'form-control textarea', 'placeholder' => 'Интересующий Вас вопрос...'])->label(false)?>
                <?=Html::submitButton('Отправить', ['class' => 'write-us-btn send-contact', 'name' => 'contact-button'])?>
                <? ActiveForm::end();?>
            </div>
            <?endif;?>
        </div>
    </div>
<div id="office" class="white-popup mfp-hide">
    <div class="about-house flex-column align-items-center d-flex container bg-white">
        <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ac769fe27cb06afd88dfcfbef78a7f52f6888c4a07f97a2bed03fb33cf3d5a894&amp;width=763&amp;height=548&amp;lang=ru_RU&amp;scroll=true"></script>    </div>
</div>
<div id="production" class="white-popup mfp-hide">
    <div class="about-house flex-column align-items-center d-flex container bg-white">
        <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ad59f04919808cd7dec2b4a47e4492dd5eb3c18c4d0f075c940813a2876b8c89b&amp;width=733&amp;height=536&amp;lang=ru_RU&amp;scroll=true"></script>
    </div>
</div>