<?php

/**
 * Created by PhpStorm.
 * User: dmitrijlebedev
 * Date: 26.05.18
 * Time: 18:27
 */
$this->title = 'Умное решение';
?>

<div class="container-fluid bg-white min-height">
    <div class="news-container container full-width">
        <? foreach ($model as $item):?>
        <h2 style="text-align: center"><?=$item->title?></h2>
            <img src="<?=$item->img?>" class="rounded mx-auto d-block" alt="">
        <?=$item->text?>
        <h5>Опубликовано: <?=$item->date?></h5>
        <?endforeach;?>
    </div>
</div>