<?php

use frontend\controllers\SiteController;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Блог';
?>
<div class="post-index">
    <div class="container-fluid bg-white">
        <div class="container gallery-wrapper">
            <div class="row justify-content-center">
                <div class="title-wrap align-items-center d-flex flex-column">
                    <div class="decoration">
                        <div></div>
                        <div></div>
                    </div>
                    <h2>Умный блог</h2>
                    <p>Узнай больше о строительстве на нашем сайте </p>
                </div>
            </div>
            <div class="card-container w-100 d-flex flex-wrap justify-content-around">
                <? foreach ($model as $item):?>
                    <?$text = mb_substr($item->short, 0, 130, 'UTF-8').'...'?>
                    <? $date = explode("-",$item->date)?>
                <div class="card card-news d-flex">
                    <div class="img-news-wrapper">
                        <img class="card-img-top" src="<?=$item->img?>" alt="Card image cap">
                    </div>
                    <span class="badge data d-flex justify-content-center align-items-center flex-column">
                        <span><?=$date[2]?></span>
                        <span><?=SiteController::dateMonth($date[1])?></span>
                    </span>
                    <div class="card-body">
                        <h3><?=$item->title?></h3>
                        <p class="card-text"><?=$text?></p>
                    </div>
                    <a href="<?=Yii::$app->request->url."?article=".$item->id?>">Читать полностью &raquo;</a>
                </div>
                <?endforeach;?>
            </div>
            <div class="row d-flex justify-content-center">
                <nav>

                    <?= \yii\widgets\LinkPager::widget([
                        'pagination' =>$page,
                        'options' => [
                            'class' => 'pagination justify-content-center',
                        ],
                        'pageCssClass' => 'page-item',
                        'linkOptions' => [
                            'class' => 'page-link'
                        ],
//                        'prevPageLabel' => false,
//                        'lastPageLabel' => false,
                        'disabledPageCssClass' => 'page-link'

                    ])?>
                </nav>
            </div>
        </div>
    </div>
</div>
