<?php
/**
 * Created by PhpStorm.
 * User: dmitrijlebedev
 * Date: 29.05.18
 * Time: 13:11
 */
?>
<div class="container-fluid bg-white">
    <div class="container gallery-wrapper">
        <div class="row justify-content-center">
            <div class="title-wrap align-items-center d-flex flex-column">
                <div class="decoration">
                    <div></div>
                    <div></div>
                </div>
                <h2>Возможные интерьеры Вашего дома</span></h2>
            </div>
        </div>
        <div class="row d-flex justify-content-center">
            <? foreach ($post as $item):?>
                <div class="img-gallery">
                    <a href="<?=$item->img?>" class="item">
                        <img src="<?=$item->img?>" alt="">
                    </a>
                </div>
            <?endforeach;?>
        </div>
        <div class="row d-flex justify-content-center">
            <nav>

                <?= \yii\widgets\LinkPager::widget([
                    'pagination' =>$page,
                    'options' => [
                        'class' => 'pagination justify-content-center',
                    ],
                    'pageCssClass' => 'page-item',
                    'linkOptions' => [
                        'class' => 'page-link'
                    ],
//                        'prevPageLabel' => false,
//                        'lastPageLabel' => false,
                    'disabledPageCssClass' => 'page-link'

                ])?>
            </nav>
        </div>
    </div>
</div>
