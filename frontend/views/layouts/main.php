<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="yandex-verification" content="f638821f19bf2b17" />
    <meta name="description" content="Производство домокомплектов для самостоятельной сборки дома. Дом типа A-frame дешевле других. В качестве фундамента будет достаточно 12 свай (диаметром 108 мм)" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700">
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter49086039 = new Ya.Metrika2({
                        id:49086039,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/tag.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks2");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/49086039" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <div class="slide-out-menu" id="slide-menu" style="margin-left: -251px">
        <nav class="nav flex-column">
            <a class="nav-link" href="<?= Yii::$app->homeUrl?>">Главная</a>
            <a class="nav-link" href="<?= Yii::$app->urlManager->createUrl('/gallery')?>">Галерея</a>
            <a class="nav-link" href="<?= Yii::$app->urlManager->createUrl('/inter')?>">Интерьеры</a>
            <a class="nav-link" href="<?= Yii::$app->urlManager->createUrl('/post')?>">Блог</a>
            <a class="nav-link" href="<?= Yii::$app->urlManager->createUrl('/contacts')?>">Контакты</a>
        </nav>
    </div>
    <header class="container-fluid header-wrapper">
        <nav class="navbar navbar-expand-xl navbar-light container header-container">
            <a class="navbar-brand" href="<?= Yii::$app->homeUrl?>">
                <img src="/img/logo.svg" width="95" height="50" alt="">
                <h1>Умное Решение</h1>
            </a>
            <button class="navbar-toggler" type="button" id="menu">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse nav-margin navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav nav-wrap" id="panel">
                    <li class="nav-item active">
                        <a class="nav-link <? if (Yii::$app->request->url == Yii::$app->homeUrl){ echo "active_link";}?>" href="<?= Yii::$app->homeUrl?>">Главная</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <? if (Yii::$app->request->url == ('/gallery')) {echo "active_link";} ?>" href="<?= Yii::$app->urlManager->createUrl('/gallery')?>">Галерея</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <? if (Yii::$app->request->url == ('/inter')){echo "active_link";}?>" href="<?= Yii::$app->urlManager->createUrl('/inter')?>">Интерьеры</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <? if (Yii::$app->request->url == ('/post')){echo "active_link";}?>" href="<?= Yii::$app->urlManager->createUrl('/post')?>">Блог</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <? if (Yii::$app->request->url == ('/contacts')){echo "active_link";}?>" href="<?= Yii::$app->urlManager->createUrl('/contacts')?>">Контакты</a>
                    </li>
                </ul>
                <a class="header-number" style="color: #000;" href="tel:+79262739920" target="_blank">8 (926) 273 99 20</a>
                <a class="what-number" style="color: #000;" href="https://api.whatsapp.com/send?phone=79262739920" target="_blank">8 (926) 273 99 20</a>
            </div>
        </nav>
    </header>
        <?= $content ?>
</div>
<footer class="container-fluid">
    <div class="container footer d-flex flex-column">
        <div class="footer-content d-flex flex-row justify-content-between">
            <div class="footer-menu">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a href="<?= Yii::$app->homeUrl?>">Главная</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= Yii::$app->urlManager->createUrl('/gallery')?>">Галерея</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= Yii::$app->urlManager->createUrl('/inter')?>">Интерьеры</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= Yii::$app->urlManager->createUrl('/post')?>">Блог</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= Yii::$app->urlManager->createUrl('/contacts')?>">Контакты</a>
                    </li>
                </ul>
            </div>
            <div class="logo-wrap d-flex flex-column justify-content-center align-items-center">
                <img src="/img/logo.svg" width="175" height="100" alt="">
                <h1>Умное Решение</h1>
            </div>
            <div class="footer-contacts d-flex flex-column">
                <table class="footer-table">
                    <tr class="footer__tr">
                        <td class="footer__td">Москва</td>
                        <td class="footer__td"><p class=" number">8 (926) 273 99 20</p></td>
                    </tr>
                    <tr class="footer__tr">
                        <td class="footer__td">Кострома</td>
                        <td class="footer__td"><p class=" number">8 (953) 668 81 38</p></td>
                    </tr>
                </table>
                <aside class="footer-social">
                    <a href="https://www.facebook.com/umnoe.reshenie" target="_blank">
                        <span class="instagram"></span>
                    </a>
                    <a href="https://vk.com/public127208078" target="_blank">
                        <span class="vk"></span>
                    </a>
                    <a href="https://www.instagram.com/a_frame_solution/" target="_blank">
                        <span class="whatsup"></span>
                    </a>
                </aside>
            </div>
        </div>
        <div class="footer-rights">
            <p class="footer-sign">© Copyright <?= date('Y') ?></p>
            <p class="footer-sign">All Rights Reserved</p>
            <p class="footer-sign"><a href="https://vk.com/dezzzi" target="_blank">Lebedev Dmitri</a></p>
            <p class="footer-sign">Kostroma</p>
        </div>
    </div>
</footer>
</main>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
