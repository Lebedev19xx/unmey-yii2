<?php
/**
 * Created by PhpStorm.
 * User: dmitrijlebedev
 * Date: 27.05.18
 * Time: 21:40
 */
namespace frontend\controllers;


use yii\web\Controller;
use frontend\models\ContactForm;
use Yii;

class ContactsController extends Controller
{


    public function actionIndex(){
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()){
            if ($model->sendEmail()){
                Yii::$app->session->setFlash('successSend');
            }else{
                Yii::$app->session->setFlash('errorSend');
            }
//            return $this->refresh();
        }
        return $this->render('index', compact('model'));
    }

}