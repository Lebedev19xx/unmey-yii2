<?php
/**
 * Created by PhpStorm.
 * User: dmitrijlebedev
 * Date: 29.05.18
 * Time: 13:10
 */

namespace frontend\controllers;


use yii\data\Pagination;
use yii\web\Controller;
use app\models\GalleryUser;

class GalleryController extends Controller
{
    public function actionIndex(){
        $model = GalleryUser::find();
        $page = new Pagination(['totalCount' => $model->count(), 'pageSize' =>16]);
        $post = $model->offset($page->offset)->limit($page->limit)->orderBy(['id' => SORT_DESC])->all();
        return $this->render('index', compact('post', 'page'));
    }
}