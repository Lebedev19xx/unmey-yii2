<?php
/**
 * Created by PhpStorm.
 * User: dmitrijlebedev
 * Date: 29.05.18
 * Time: 20:01
 */

namespace frontend\controllers;


use yii\web\Controller;
use app\models\InterUser;
use yii\data\Pagination;

class InterController extends Controller
{
    public function actionIndex(){
        $model = InterUser::find();
        $page = new Pagination(['totalCount' => $model->count(), 'pageSize' =>16]);
        $post = $model->offset($page->offset)->limit($page->limit)->orderBy(['id' => SORT_DESC])->all();
        return $this->render('index', compact('post', 'page'));
    }
}