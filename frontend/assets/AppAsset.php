<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap-grid.css',
        'css/bootstrap.css',
        'css/bootstrap-reboot.css',
        'css/magnific-popup.css',
        'css/style.css',

    ];
    public $js = [
        'https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js',
        'js/jquery.magnific-popup.js',
        'js/js.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
