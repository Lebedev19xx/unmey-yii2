<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $phone;
//    public $subject;
    public $body;
//    public $email;
//    public $verifyCode;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'phone', 'body'], 'required'],
            [['phone'], 'integer']
            // email has to be a valid email address
//            [['email'], 'email']
            // verifyCode needs to be entered correctly
//            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
//        return [
//            'verifyCode' => 'Verification Code',
//        ];
    }



    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
//     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function sendEmail()
    {
           return Yii::$app->mailer->compose()
                ->setTo('umnye2016@gmail.com')
                ->setFrom('smart@ch67454.tmweb.ru')
                ->setSubject('Новое уведомление с сайта "Умное Решение" от пользователя '.$this->name.', номер:'.$this->phone)
                ->setTextBody($this->body.'. Перезвоните пожалуйста по этому номеру: '.$this->phone)
                ->send();

    }
}
