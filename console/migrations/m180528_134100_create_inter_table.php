<?php

use yii\db\Migration;

/**
 * Handles the creation of table `inter`.
 */
class m180528_134100_create_inter_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('inter', [
            'id' => $this->primaryKey(),
            'img' =>$this->string(255)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('inter');
    }
}
