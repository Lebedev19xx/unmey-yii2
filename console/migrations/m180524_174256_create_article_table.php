<?php

use yii\db\Migration;

/**
 * Handles the creation of table `article`.
 */
class m180524_174256_create_article_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('article', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'short' => $this->text(),
            'text' => $this->text(),
            'img' => $this->string(),
            'date' => $this->date()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('article');
    }
}
