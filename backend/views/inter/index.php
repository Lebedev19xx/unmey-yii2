<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Управление интерьерами';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inter-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить изображение', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute'=>'img',
                'format' => 'raw',
                'value' => function($data) {
                    return Html::img($data->img, ['style'=>'width: 10%;']);
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
