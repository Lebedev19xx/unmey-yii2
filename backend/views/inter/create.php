<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Inter */

$this->title = 'Добавить изображение';
$this->params['breadcrumbs'][] = ['label' => 'Inters', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inter-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
