<?php

/* @var $this yii\web\View */

$this->title = 'Админ панель Умное Решение';
?>
<div class="site-index">

    <div class="body-content">
        <div class="row">
            <a href="/admin/article">Управление статьями</a>
        </div>

        <div class="row">
            <a href="/admin/gallery">Управление галереей</a>
        </div>
        <div class="row">
            <a href="/admin/inter">Управление интерьерами</a>
        </div>

    </div>
</div>
