<?php

namespace backend\controllers;

use Yii;
use app\models\Article;
use app\models\ArticleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Article();

        if ($model->load(Yii::$app->request->post())){
            //получаю информацию о файле
            $file = UploadedFile::getInstance($model, 'img');
            if ($file && $file->tempName){
                $model->img = $file;
                //валидирую
                if ($model->validate('img')){
                    //алиас для записи в базу данных
                    $dir = Yii::getAlias('img/article/');
                    // алиас для сохранения картинки во фронт
                    $dirSave = Yii::getAlias('@frontend/web/img/article/');
                    // разбиваю имя и тип
                    $type = explode('.', $model->img);
                    //переимяновываю
                    $fileName = time(). ".".$type[1];
                    //сохраняю в директорию
                    $model->img->saveAs($dirSave.$fileName);
                    //сохраняю в базу
                    $model->img = '/'.$dir.$fileName;
                }

            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }


        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())){
            //получаю информацию о файле
            $file = UploadedFile::getInstance($model, 'img');
            if ($file && $file->tempName){
                $model->img = $file;
                //валидирую
                if ($model->validate('img')){
                    //алиас для записи в базу данных
                    $dir = Yii::getAlias('img/article/');
                    // алиас для сохранения картинки во фронт
                    $dirSave = Yii::getAlias('@frontend/web/img/article/');
                    // разбиваю имя и тип
                    $type = explode('.', $model->img);
                    //переимяновываю
                    $fileName = time(). ".".$type[1];
                    //сохраняю в директорию
                    $model->img->saveAs($dirSave.$fileName);
                    //сохраняю в базу
                    $model->img = '/'.$dir.$fileName;
                }

            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
